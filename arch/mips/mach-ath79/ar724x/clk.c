
#include <common.h>
#include <clock_legacy.h>
#include <asm/io.h>
#include <asm/addrspace.h>
#include <asm/types.h>
#include <mach/ath79.h>
#include <mach/ar7240_soc.h>

DECLARE_GLOBAL_DATA_PTR;
 

void 
ar7240_sys_frequency(u32 *cpu_freq, u32 *ddr_freq, u32 *ahb_freq)
{
    u32 pll, pll_div, ref_div, ahb_div, ddr_div, freq;

    pll = ar7240_reg_rd(AR7240_CPU_PLL_CONFIG);

    pll_div = 
        ((pll & AR7240_PLL_CONFIG_PLL_DIV_MASK) >> AR7240_PLL_CONFIG_PLL_DIV_SHIFT);

    ref_div =
        ((pll & AR7240_PLL_CONFIG_PLL_REF_DIV_MASK) >> AR7240_PLL_CONFIG_PLL_REF_DIV_SHIFT);

    ddr_div = 
        ((pll & AR7240_PLL_CONFIG_DDR_DIV_MASK) >> AR7240_PLL_CONFIG_DDR_DIV_SHIFT) + 1;

    ahb_div = 
       (((pll & AR7240_PLL_CONFIG_AHB_DIV_MASK) >> AR7240_PLL_CONFIG_AHB_DIV_SHIFT) + 1)*2;

    freq = pll_div * ref_div * 5000000; 

	gd->cpu_clk = freq;
    if (cpu_freq){
        *cpu_freq = gd->cpu_clk;
    }
	gd->mem_clk = freq/ddr_div;
    if (ddr_freq){
        *ddr_freq = gd->mem_clk;
    }

	gd->bus_clk = freq/ahb_div;
    if (ahb_freq) {
        *ahb_freq = gd->bus_clk;
    }
}



static u32 ar724x_get_xtal(void)
{
	return 40000000;
}


int get_clocks(void)
{
	u32 ahb_freq, ddr_freq, cpu_freq;

    ar7240_sys_frequency(&cpu_freq, &ddr_freq, &ahb_freq);  
	
	return 0;
}

 


int get_serial_clock(void)
{
	return ar724x_get_xtal();
}

ulong get_bus_freq(ulong dummy)
{
	if (!gd->bus_clk)
		get_clocks();
	return gd->bus_clk;
}

ulong get_ddr_freq(ulong dummy)
{
	if (!gd->mem_clk)
		get_clocks();
	return gd->mem_clk;
}
