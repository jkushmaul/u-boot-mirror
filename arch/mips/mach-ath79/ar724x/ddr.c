// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2015-2016 Wills Wang <wills.wang@live.com>
 * Based on Atheros LSDK/QSDK
 */

#include <common.h>
#include <asm/io.h>
#include <asm/addrspace.h>
#include <asm/types.h>
#include <mach/ath79.h>
#include <mach/ar7240_soc.h>

#define AR7240_DDR_SIZE_INCR    (4*1024*1024)
int
ar7240_ddr_find_size(void)
{
    uint8_t  *p = (uint8_t *)KSEG1, pat = 0x77;
    int i;

    *p = pat;

    for(i = 1; ; i++) {
        *(p + i * AR7240_DDR_SIZE_INCR) = (uint8_t)(i);
        if (*p != pat) {
            break;
        }
    }

    return (i*AR7240_DDR_SIZE_INCR);
}

void ar7240_ddr_tap_tuning(void)
{
    unsigned int tap_val1, tap_val2;
 	
	ar7240_reg_wr (AR7240_DDR_TAP_CONTROL0, 0x7);
    ar7240_reg_wr (AR7240_DDR_TAP_CONTROL1, 0x7);

	tap_val1 = ar7240_reg_rd(0xb800001c);
    tap_val2 = ar7240_reg_rd(0xb8000020);
    printf("#### TAP VALUE 1 = %x, 2 = %x\n",tap_val1, tap_val2);
}

void ddr_tap_tuning(void)
{
	ar7240_ddr_tap_tuning();
}
 
void ar7240_ddr_init_uncompressed(int* mod_val, uint32_t refresh)
{
	int ddr2 = 0,ddr_config;
	int ddr_config2,ext_mod,ddr2_ext_mod;
	int mod_val_init;

	if (is_ar7241() || is_ar7242()) {
			if (ddr2) {
//				prmsg("%s(%d): virian ddr2 init\n", __func__, __LINE__);
				ddr_config	= CFG_7241_DDR2_CONFIG_VAL;
				ddr_config2	= CFG_7241_DDR2_CONFIG2_VAL;
				ext_mod		= CFG_7241_DDR2_EXT_MODE_VAL;
				ddr2_ext_mod	= CFG_DDR2_EXT_MODE_VAL;
				mod_val_init	= CFG_7241_DDR2_MODE_VAL_INIT;
				mod_val		= (int*) CFG_7241_DDR2_MODE_VAL;
			} else {
//				prmsg("%s(%d): virian ddr1 init\n", __func__, __LINE__);
				ddr_config	= CFG_7241_DDR1_CONFIG_VAL;
				ddr_config2	= CFG_7241_DDR1_CONFIG2_VAL;
				ext_mod		= CFG_7241_DDR1_EXT_MODE_VAL;
				ddr2_ext_mod	= CFG_DDR2_EXT_MODE_VAL;
				mod_val_init	= CFG_7241_DDR1_MODE_VAL_INIT;
				mod_val		= (int*) CFG_7241_DDR1_MODE_VAL;
			}
	} else {
		//prmsg("%s(%d): python ddr init\n", __func__, __LINE__);
		ddr_config = CFG_DDR_CONFIG_VAL;
		ddr_config2 = CFG_DDR_CONFIG2_VAL;
		ext_mod = CFG_DDR_EXT_MODE_VAL;
#ifndef CONFIG_WASP_EMU
        ddr2_ext_mod = CFG_DDR2_EXT_MODE_VAL;
#endif
		mod_val_init = CFG_DDR_MODE_VAL_INIT;
		mod_val = (int*) CFG_DDR_MODE_VAL;
	}

	if (ddr2) {
		ar7240_reg_wr_nf(0xb800008c, 0xA59);
		udelay(100);
		ar7240_reg_wr_nf(AR7240_DDR_CONTROL, 0x10);
		udelay(10);
		ar7240_reg_wr_nf(AR7240_DDR_CONTROL, 0x20);
		udelay(10);
	}

	ar7240_reg_wr_nf(AR7240_DDR_CONFIG, ddr_config);
	udelay(100);
	ar7240_reg_wr_nf(AR7240_DDR_CONFIG2, ddr_config2 | 0x80);
	udelay(100);
	ar7240_reg_wr_nf(AR7240_DDR_CONTROL, 0x8);
	udelay(10);

	ar7240_reg_wr_nf(AR7240_DDR_MODE, mod_val_init);
	udelay(1000);

	ar7240_reg_wr_nf(AR7240_DDR_CONTROL, 0x1);
	udelay(10);

	if (ddr2) {
		ar7240_reg_wr_nf(AR7240_DDR_EXT_MODE, ddr2_ext_mod);
	} else {
		ar7240_reg_wr_nf(AR7240_DDR_EXT_MODE, ext_mod);
	}  
}

#ifdef COMPRESSED_UBOOT
void ar7240_ddr_init_compressed(int mod_val, uint32_t refresh)
{
	
}
#endif

void ddr_init(void)
{
	int mod_val = CFG_DDR_MODE_VAL;
	uint32_t refresh = CFG_DDR_REFRESH_VAL;
#ifdef COMPRESSED_UBOOT
  	pragma error("ar724x COMPRESSED_BOOT not supported")
	ar7240_ddr_init_compressed(mod_val, refresh);
#else
	ar7240_ddr_init_uncompressed(&mod_val, refresh);
#endif

	udelay(100);
	ar7240_reg_wr_nf(AR7240_DDR_CONTROL, 0x2);
	udelay(10);
	ar7240_reg_wr_nf(AR7240_DDR_CONTROL, 0x8);
	udelay(10);
	ar7240_reg_wr_nf(AR7240_DDR_MODE, mod_val);
	udelay(100);
	ar7240_reg_wr_nf(AR7240_DDR_CONTROL, 0x1);
	udelay(10);
	ar7240_reg_wr_nf(AR7240_DDR_REFRESH, refresh);
	udelay(100);
	ar7240_reg_wr_nf(AR7240_DDR_RD_DATA_THIS_CYCLE,
				CFG_DDR_RD_DATA_THIS_CYCLE_VAL);
	udelay(100);
}
