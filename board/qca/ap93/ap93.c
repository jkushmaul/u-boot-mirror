// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2015-2016 Wills Wang <wills.wang@live.com>
 */

#include <config.h>

#include <common.h>
#include <asm/io.h>
#include <asm/addrspace.h>
#include <asm/types.h>
#include <mach/ar71xx_regs.h>
#include <mach/ddr.h>
#include <mach/ath79.h>
#include <debug_uart.h>
 
void ar724x_gpio_config_iomem(void __iomem*regs);

void ar724x_gpio_config(void)
{
   
    void __iomem *regs = (void __iomem*) (KSEG1ADDR(AR71XX_GPIO_BASE));

 	ar724x_gpio_config_iomem(regs);
}
 

int board_early_init_f(void)
{
	//necessary to get serial enabled early enough.
	//I can't seem to get things to work without this first.
	ar724x_gpio_config();
	

	ddr_init();
	
	ath79_usb_reset();
	ath79_eth_reset();
	return 0;
}
