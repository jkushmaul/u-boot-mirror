#include <common.h>
#include <clock_legacy.h>
#include <dm.h>
#include <div64.h>
#include <errno.h>
#include <serial.h>
#include <asm/io.h>
#include <asm/addrspace.h>
#include <asm/types.h>
#include <dm/pinctrl.h>
#include <mach/ar71xx_regs.h>

void 
ar7240_sys_frequency(u32 *cpu_freq, u32 *ddr_freq, u32 *ahb_freq);
 
/* === END OF CONFIG === */

/* register offset */
#define         OFS_RCV_BUFFER          0x00
#define         OFS_TRANS_HOLD          0x00
#define         OFS_SEND_BUFFER         0x00 //?
#define         OFS_DIVISOR_LSB         0x00

#define         OFS_DIVISOR_MSB         0x04
#define         OFS_INTR_ENABLE         0x04

#define         OFS_INTR_ID             0x08
#define         OFS_DATA_FORMAT         0x0c
#define         OFS_LINE_CONTROL        0x0c
#define         OFS_MODEM_CONTROL       0x10
#define         OFS_RS232_OUTPUT        0x10
#define         OFS_LINE_STATUS         0x14
#define         OFS_MODEM_STATUS        0x18
#define         OFS_RS232_INPUT         0x18





#define AR724X_UART_LSR_RX          BIT(0)
#define AR724X_UART_LSR_TX          BIT(5)

#define AR724X_UART_LCR_DLAB        BIT(7)
#define AR724X_UART_LCR_BREAK       BIT(6)
//5 - RES
#define AR724X_UART_LCR_EPS         BIT(4)
#define AR724X_UART_LCR_PEN         BIT(3)
#define AR724X_UART_LCR_STOP        BIT(2)
#define AR724X_UART_LCR_CLS_5       BIT(0)
#define AR724X_UART_LCR_CLS_6       BIT(1)
#define AR724X_UART_LCR_CLS_7       BIT(2)
#define AR724X_UART_LCR_CLS_8       (BIT(1) | BIT(2))

struct ar724x_serial_priv {
	void __iomem *regs;
};


static inline int ar724x_serial_setbrg_iomem(void __iomem *regs, u32 ahb_freq, int baudrate)
{ 
    u32 div;
    div  = ahb_freq/(16 * baudrate);  

    // set DIAB bit 
    writel(AR724X_UART_LCR_DLAB, regs + OFS_LINE_CONTROL);
      
    // set divisor
    writel((div & 0xff), regs + OFS_DIVISOR_LSB);
    writel(((div >> 8) & 0xff),regs + OFS_DIVISOR_MSB);

    // clear DIAB bit
    writel(0x00, regs + OFS_LINE_CONTROL);

    return 0;
}

static inline int ar724x_serial_pending_iomem(void __iomem *regs, bool input)
{
    int line_status = readl(regs + OFS_LINE_STATUS);

	if (input)
    {
        //data in this register is only valid if the Data Ready (DR) bit in the Line Status Register (LSR) is set.
        //if set, data is pending
		return (line_status & AR724X_UART_LSR_RX) ? 1 : 0;
    }
	else
    {
        //Data can be written to the THR any time the THR Empty (THRE) bit of the Line Status Register is set
        //if set, data is not pending
		return (line_status & AR724X_UART_LSR_TX) ? 0 : 1 ;
    }
}

static int ar724x_serial_getc_iomem(void __iomem *regs)
{
    //If there is not data pending to be read, -EAGAIN
    if (!ar724x_serial_pending_iomem(regs, true)) {
        return -EAGAIN;
    }
    return readl(regs + OFS_RCV_BUFFER);
}

static inline int ar724x_serial_putc_iomem(void __iomem *regs, const char ch)
{
    //If there is data pending to be written, -EAGAIN
    if (ar724x_serial_pending_iomem(regs, false)) { //there *was* data waiting to be output
        return -EAGAIN;
    }
    
    writel(ch, regs + OFS_SEND_BUFFER);
    return 0;
}


static inline int ar724x_serial_init_iomem(void __iomem *regs)
{

    u32 ahb_freq, ddr_freq, cpu_freq;
    ar7240_sys_frequency(&cpu_freq, &ddr_freq, &ahb_freq);  
    ar724x_serial_setbrg_iomem(regs, ahb_freq, CONFIG_BAUDRATE);

    // set data format
    //UART16550_WRITE(OFS_DATA_FORMAT, 0x3);
    writel(0x3, regs + OFS_DATA_FORMAT);

    //UART16550_WRITE(OFS_INTR_ENABLE, 0);
    writel(0x0, regs + OFS_INTR_ENABLE);

    return 0;
}


/***********DM************/

 static int ar724x_serial_setbrg(struct udevice *dev, int baudrate)
{
    return 0;
}

 

static int ar724x_serial_pending(struct udevice *dev, bool input)
{
	struct ar724x_serial_priv *priv = dev_get_priv(dev);
    return ar724x_serial_pending_iomem(priv->regs, input);
}

 

static int ar724x_serial_putc(struct udevice *dev, const char ch)
{
    struct ar724x_serial_priv *priv = dev_get_priv(dev);
    return ar724x_serial_putc_iomem(priv->regs, ch);
}
 

static int ar724x_serial_getc(struct udevice *dev)
{
    struct ar724x_serial_priv *priv = dev_get_priv(dev);
    return ar724x_serial_getc_iomem(priv->regs);
}

static int ar724x_serial_probe(struct udevice *dev)
{
    struct ar724x_serial_priv *priv = dev_get_priv(dev);
	fdt_addr_t addr;

	addr = devfdt_get_addr(dev);
	if (addr == FDT_ADDR_T_NONE)
		return -EINVAL;

	priv->regs = map_physmem(addr, AR724X_UART_SIZE,
				 MAP_NOCACHE);
 
 
    
    return ar724x_serial_init_iomem(priv->regs);
}

static const struct dm_serial_ops ar724x_serial_ops = {
	.putc = ar724x_serial_putc,
	.pending = ar724x_serial_pending,
	.getc = ar724x_serial_getc,
	.setbrg = ar724x_serial_setbrg,
};

static const struct udevice_id ar724x_serial_ids[] = {
	{ .compatible = "qca,ar724x-uart" },
	{  }
};

U_BOOT_DRIVER(serial_ar724x) = {
	.name = "serial_ar724x",
	.id = UCLASS_SERIAL,
	.of_match = ar724x_serial_ids,
	.probe = ar724x_serial_probe,
	.priv_auto_alloc_size = sizeof(struct ar724x_serial_priv),
	.ops = &ar724x_serial_ops,
};


#ifdef CONFIG_DEBUG_UART
#include <debug_uart.h>


void _debug_uart_init(void)
{
	void __iomem *regs = (void __iomem*) (CONFIG_DEBUG_UART_BASE);
	ar724x_serial_setbrg_iomem(regs, CONFIG_DEBUG_UART_CLOCK, CONFIG_BAUDRATE);
    ar724x_serial_init_iomem(regs);
}

void _debug_uart_putc(int c)
{
	void __iomem *regs = (void __iomem*) (CONFIG_DEBUG_UART_BASE);

    while(ar724x_serial_pending_iomem(regs, false));
    ar724x_serial_putc_iomem(regs, c & 0xff);
}

 
DEBUG_UART_FUNCS
#endif