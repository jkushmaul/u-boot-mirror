// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2015-2016 Wills Wang <wills.wang@live.com>
 */

#include <common.h>
#include <dm.h>
#include <errno.h>
#include <asm/io.h>
#include <dm/pinctrl.h>
#include <mach/ar71xx_regs.h>



#ifdef CONFIG_DEBUG_UART
#include <debug_uart.h>
#define debug_log(x) printascii(x)
#define debug_log_dec(x) printdec(x)
#else
#define debug_log(x)
#define debug_log_dec(x)
#endif

DECLARE_GLOBAL_DATA_PTR;

enum periph_id {
	PERIPH_ID_UART0,
	PERIPH_ID_SPI0,
	PERIPH_ID_NONE = -1,
};

struct ar724x_pinctrl_priv {
	void __iomem *regs;
};

 	

static const char * const ar724x_groups[] = {
	"uart",
};

static const char * ar724x_functions[] = {
	"uart",
};


static int ar724x_get_groups_count(struct udevice *dev)
{
	debug_log("ar724x_get_groups_count()\n");
	return ARRAY_SIZE(ar724x_groups);
}

static const char *ar724x_get_group_name(struct udevice *dev,
					  unsigned selector)
{
	debug_log("ar724x_get_group_name()\n");
	return ar724x_groups[selector];
}

static int ar724x_get_functions_count(struct udevice *dev)
{
	debug_log("ar724x_get_functions_count()\n");
	return ARRAY_SIZE(ar724x_functions);
}

static const char *ar724x_get_function_name(struct udevice *dev,
					     unsigned selector)
{
	debug_log("ar724x_get_function_name()\n");
	return ar724x_functions[selector];
}

static int ar724x_pinmux_group_set(struct udevice *dev,
				    unsigned group_selector,
				    unsigned func_selector)
{
	debug("ar724x pinmux: group = %d (%s), function = %d (%s)\n",
	      group_selector, ar724x_get_group_name(dev, group_selector),
	      func_selector, ar724x_get_function_name(dev, func_selector));

	return 0;
}

static inline void
ar7240_gpio_out_val_iomem(void __iomem *regs, int gpio, int val)
{
    if (val & 0x1) {
        setbits_be32(regs + AR71XX_GPIO_REG_OUT, (1 << gpio));
    }
    else {
        clrbits_be32(regs + AR71XX_GPIO_REG_OUT, (1 << gpio));
    }
}


static inline void 
ar7242x_gpio_spi_config_iomem(void __iomem *regs, int cs)
{
	debug_log("ar7242x_gpio_spi_config_iomem()\n");
	switch (cs) {
	case 0:
		debug_log("case 0\n");
		clrsetbits_be32(regs + AR71XX_GPIO_REG_OE,
				AR933X_GPIO(4), AR933X_GPIO(3) |
				AR933X_GPIO(5) | AR933X_GPIO(2));

		setbits_be32(regs + AR71XX_GPIO_REG_FUNC,
			     AR724X_GPIO_FUNC_SPI_EN |
			     AR724X_GPIO_FUNC_RES_TRUE);
		break;
	}
}

static inline void 
ar7240_gpio_uart_iomem(void __iomem *regs)
{

    clrsetbits_be32(regs + AR71XX_GPIO_REG_OE,
        AR724X_GPIO_IN_MUX_UART0_SIN,
        AR724X_GPIO_IN_MUX_UART0_RES_TRUE |
		    AR724X_GPIO_IN_MUX_UART0_SOUT);
	setbits_be32(regs + AR71XX_GPIO_REG_FUNC,
		AR724X_GPIO_FUNC_UART_EN |
		    AR724X_GPIO_FUNC_RES_TRUE);
}

static inline void
ar7240_gpio_disable_clock_obs_iomem(void __iomem *regs)
{
	//Disable clock obs 
    const u32 mask = 0xffe7e0ff;
    /*
    This gets value from gpio func1 and clears bits 8-12,19-20
    8-12: reserved set to 0
    19-20: reserved 
    */
    u32 val;
    val = readl(regs + AR71XX_GPIO_REG_FUNC) & mask;
    writel(val, regs + AR71XX_GPIO_REG_FUNC);
}

void ar724x_gpio_config_iomem(void __iomem* regs)
{
   #define GPIO_RED_STATUS_LED	0
   ar7240_gpio_out_val_iomem(regs, GPIO_RED_STATUS_LED, 1);
   ar7240_gpio_uart_iomem(regs);
   ar7240_gpio_disable_clock_obs_iomem(regs);
   ar7242x_gpio_spi_config_iomem(regs, 0);
}

static int ar724x_pinctrl_request(struct udevice *dev, int func, int flags)
{
	struct ar724x_pinctrl_priv *priv = dev_get_priv(dev);

	debug_log("ar724x_pinctrl_request()\n");

	switch (func) {
	case PERIPH_ID_SPI0:
		ar7242x_gpio_spi_config_iomem(priv->regs, flags);
		break;
	case PERIPH_ID_UART0:
		debug_log("ar724x_pinctrl_request() PERIPH_ID_UART0\n");
		ar7240_gpio_uart_iomem(priv);
		break;
	default:
		debug_log("ar724x_pinctrl_request() EINVAL\n");
		return -EINVAL;
	}
	debug_log("ar724x_pinctrl_request() returning\n");
	return 0;
}

static int ar724x_pinctrl_get_periph_id(struct udevice *dev,
					struct udevice *periph)
{
	u32 cell[2];
	int ret;
	debug_log("ar724x_pinctrl_get_periph_id(): ");
	debug_log(periph->name);
	debug_log("\n");

	debug_log("dev_of_offset(periph): ");
	debug_log_dec(dev_of_offset(periph));
	debug_log("\n");
	
	ret = fdtdec_get_int_array(gd->fdt_blob, dev_of_offset(periph),
				   "interrupts", cell, ARRAY_SIZE(cell));
	if (ret < 0){ 
		debug_log("ar724x_pinctrl_get_periph_id() fdtdec not found\n");
		return -EINVAL;
	}
	debug_log("ret=");
	debug_log_dec(ret);
	debug_log("\n");
	for(int i = 0; i < 0; i++) {
		debug_log("cell[");
		debug_log_dec(i);
		debug_log("]=");
		debug_log_dec(cell[i]);
		debug_log("\n");
	}
	
	
	
	switch (cell[0]) {
	case 0x80:  //Magic!: 0x80
		debug_log("ar724x_pinctrl_get_periph_id() returning 128\n");
		return PERIPH_ID_UART0;
	case 0x81:  //Magic!: 0x81
		return PERIPH_ID_SPI0;
	}
	debug_log("ar724x_pinctrl_get_periph_id() ENOENT\n");
	return -ENOENT;
}

static int ar724x_pinctrl_set_state(struct udevice *dev, struct udevice *config)
{
	debug_log("ar724x_pinctrl_set_state(): ");
	debug_log(config->name);
	debug_log("\n");

	 

	debug_log("dev_of_offset(periph): ");
	debug_log_dec(dev_of_offset(config));
	debug_log("\n"); 

	return 0;
}

static int ar724x_pinctrl_set_state_simple(struct udevice *dev,
					   struct udevice *periph)
{
	int func;

	debug_log("ar724x_pinctrl_set_state_simple()\n");

	func = ar724x_pinctrl_get_periph_id(dev, periph);
	if (func < 0) {
		debug_log("ar724x_pinctrl_set_state_simple() < 0\n");
		return func;
	}
	return ar724x_pinctrl_request(dev, func, 0);
}


static int ar724x_pinctrl_probe(struct udevice *dev)
{
	struct ar724x_pinctrl_priv *priv = dev_get_priv(dev);
	fdt_addr_t addr;

	debug_log("ar724x_pinctrl_probe()\n");

	addr = devfdt_get_addr(dev);
	if (addr == FDT_ADDR_T_NONE) {
		debug_log("ar724x_pinctrl_probe() EINVAL\n");
		return -EINVAL;
	}

	priv->regs = map_physmem(addr,
				 AR71XX_GPIO_SIZE,
				 MAP_NOCACHE);
	debug_log("ar724x_pinctrl_probe() done\n");
	return 0;
}
 

static const struct udevice_id ar724x_pinctrl_ids[] = {
	{ .compatible = "qca,ar724x-pinctrl" },
	{ }
};

static struct pinctrl_ops ar724x_pinctrl_ops = {
	.set_state_simple	= ar724x_pinctrl_set_state_simple,
	.set_state = ar724x_pinctrl_set_state,
	.request	= ar724x_pinctrl_request,
	.get_periph_id	= ar724x_pinctrl_get_periph_id,
	.get_groups_count = ar724x_get_groups_count,
	.get_group_name = ar724x_get_group_name,
	.get_functions_count = ar724x_get_functions_count,
	.get_function_name = ar724x_get_function_name,
	.pinmux_group_set = ar724x_pinmux_group_set,
};

U_BOOT_DRIVER(pinctrl_ar724x) = {
	.name					= "pinctrl_ar724x",
	.id						= UCLASS_PINCTRL,
	.of_match				= ar724x_pinctrl_ids,
	.priv_auto_alloc_size 	= sizeof(struct ar724x_pinctrl_priv),
	.ops					= &ar724x_pinctrl_ops,
	.probe					= ar724x_pinctrl_probe,
	
};


