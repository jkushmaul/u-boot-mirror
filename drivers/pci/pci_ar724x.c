/*****************************************************************************/
/*! file ap93_pci.c
** /brief PCI support for AP91/93 board
**
**  This provides the support code required for PCI support on the AP91/93
**  board in the U-Boot environment.  This board is a Python based system
**  with a Merlin WLAN interface.  This file also contains the support
**  for initialization of the Merlin radios on the PCi bus, required for
**  pre-configuration for use by Linux.
**
**  Copyright (c) 2008 Atheros Communications Inc.  All rights reserved.
**
*/


#include <common.h>
#include <dm.h>
#include <fdtdec.h>
#include <pci.h>


struct ar724x_pci_priv {
	struct {
		u16 vendor;
		u16 device;
	} vendev[256];
};


/*
#include <common.h>
#include <command.h>
#include <asm/mipsregs.h>
#include <asm/addrspace.h>
#include <config.h>
#include <version.h>
#include <pci.h>
#include <mach/ar7240_soc.h>

 
static struct pci_controller hose;

 
 

static int
ar7240_local_write_config(int where, int size, uint32_t value)
{
    ar7240_reg_wr((AR7240_PCI_CRP + where),value);
    return 0;
}

static int
ar7240_pci_read_config(struct pci_controller *hose,
                           pci_dev_t dev, int where, uint32_t *value)
{
	// for PCIe, return all ones for devices > 0 unless they are on a bridge
	if (((PCI_BUS(dev) - hose->first_busno) == 0) && (PCI_DEV(dev) != 0)) {
		*value = 0xffffffff;
	} else {
        *value = ar7240_reg_rd(AR7240_PCI_DEV_CFGBASE + where);
	}
        return 0;
}

static int
ar7240_pci_write_config(struct pci_controller *hose,
                           pci_dev_t dev, int where,  uint32_t value)
{
        ar7240_reg_wr((AR7240_PCI_DEV_CFGBASE + where),value);
        return 0;
}

 

void plat_dev_init(void)
{ 
 
 
    if ((is_ar7241() || is_ar7242()) || is_wasp())
        ar7240_pci_write_config(&hose, (pci_dev_t)NULL, 0x10, 0x1000ffff);
    else
        ar7240_pci_write_config(&hose, (pci_dev_t)NULL, 0x10, 0xffff);

    ar7240_pci_write_config(&hose, (pci_dev_t)NULL, 0x04, 0x6);
 
    return;
}
 
*/



static int ar724x_pci_dm_read_config(const struct udevice *dev, pci_dev_t bdf,
				      uint offset, ulong *value,
				      enum pci_size_t size)
{
	struct ar724x_pci_priv *priv = dev_get_priv(dev);
	u32 addr;

     (((PCI_BUS(dev) - hose->first_busno) == 0) && (PCI_DEV(dev) != 0)) {
		*value = 0xffffffff;
	} else {
        *value = ar7240_reg_rd(AR7240_PCI_DEV_CFGBASE + where);
	}

	addr = bdf | (offset & 0xfc) | ((offset & 0xf00) << 16) | 0x80000000;
	out_be32(priv->cfg_addr, addr);
	sync();
	*value = pci_conv_32_to_size(in_le32(priv->cfg_data), offset, size);

	return 0;
}

static int ar724x_pci_dm_write_config(struct udevice *dev, pci_dev_t bdf,
				       uint offset, ulong value,
				       enum pci_size_t size)
{
	struct ar724x_pci_priv *priv = dev_get_priv(dev);
	u32 addr;

	addr = bdf | (offset & 0xfc) | ((offset & 0xf00) << 16) | 0x80000000;
	out_be32(priv->cfg_addr, addr);
	sync();
	out_le32(priv->cfg_data, pci_conv_size_to_32(0, value, offset, size));

	return 0;
}


static inline int  ar724x_pci_dm_init_iomem(struct pci_region *dev,
                              struct pci_region *io,
                              struct pci_region *mem,
                              struct pci_region *pre)
{
    uint32_t cmd; 
    cmd = PCI_COMMAND_MEMORY | PCI_COMMAND_MASTER | PCI_COMMAND_INVALIDATE |
          PCI_COMMAND_PARITY|PCI_COMMAND_SERR|PCI_COMMAND_FAST_BACK;
 
    ar7240_reg_wr(AR7240_PCIE_PLL_CONFIG,0x02050800);

    ar7240_reg_wr(AR7240_PCIE_PLL_CONFIG,0x00050800);
    udelay(100);

    ar7240_reg_wr(AR7240_PCIE_PLL_CONFIG,0x00040800);
    udelay(100);
 

    ar7240_reg_rmw_clear(AR7240_RESET,AR7240_RESET_PCIE_PHY_SERIAL);
    udelay(100);

    ar7240_reg_rmw_clear(AR7240_RESET,AR7240_RESET_PCIE_PHY);


    ar7240_reg_rmw_clear(AR7240_RESET,AR7240_RESET_PCIE);

 

    ar7240_local_write_config(PCI_COMMAND, 4, cmd);
    ar7240_local_write_config(0x20, 4, 0x1ff01000);
    ar7240_local_write_config(0x24, 4, 0x1ff01000);

    if (ar7240_reg_rd(AR7240_PCI_LCL_RESET) != 0x7) {
        udelay(100000);
        ar7240_reg_wr_nf(AR7240_PCI_LCL_RESET, 0);
        udelay(100);
        ar7240_reg_wr_nf(AR7240_PCI_LCL_RESET, 4);
        udelay(100000);
    }

    if ((is_ar7241() || is_ar7242() || is_wasp())) {
         ar7240_reg_wr(0x180f0000, 0x1ffc1); 
    }
    else {    
        ar7240_reg_wr(0x180f0000, 0x1);
    }
 
    udelay(1000);

 

    if (((ar7240_reg_rd(AR7240_PCI_LCL_RESET)) & 0x1) == 0x0) {
        return;
    }
 
    hose.first_busno = 0; 
 
    pci_set_region( &hose.regions[0],
    	            0x80000000,
    	            0x00000000,
    	            32 * 1024 * 1024,
    	            PCI_REGION_MEM | PCI_REGION_SYS_MEMORY);

 
    pci_set_region( &hose.regions[1],
    	            0x10000000,
    	            0x10000000,
    	            128 * 1024 * 1024,
    	            PCI_REGION_MEM);

    hose.region_count = 2;

    pci_register_hose(&hose);

    pci_set_ops(&hose,
    	pci_hose_read_config_byte_via_dword,
    	pci_hose_read_config_word_via_dword,
    	ar7240_pci_read_config,
    	pci_hose_write_config_byte_via_dword,
    	pci_hose_write_config_word_via_dword,
    	ar7240_pci_write_config);

    plat_dev_init();
    pci_bus_scan(0);
 
    return 0; 
}
 

static int ar724x_pci_dm_probe(struct udevice *dev)
{
	struct ar724x_pci_priv *priv = dev_get_priv(dev);
	struct pci_region *io;
	struct pci_region *mem;
	struct pci_region *pre;
	int count;


	count = pci_get_regions(dev, &io, &mem, &pre);
	if (count != 2) {
		printf("%s: wrong count of regions %d only 2 allowed\n",
		       __func__, count);
		return -EINVAL;
	}
    return ar724x_pci_dm_init(dev, io, mem, pre);
}


static const struct dm_pci_ops ar724x_pci_ops = {
	.read_config = ar724x_pci_read_config,
	.write_config = ar724x_pci_write_config,
};

static const struct udevice_id ar724x_pci_ids[] = {
	{ .compatible = "ar724x,pci" },
	{ }
};

U_BOOT_DRIVER(pci_ar724x) = {
	.name	= "pci_ar724x",
	.id	= UCLASS_PCI,
	.of_match = ar724x_pci_ids,
	.ops	= &ar724x_pci_ops,
	.probe	= ar724x_pci_probe,
	.priv_auto_alloc_size = sizeof(struct ar724x_pci_priv),

	/* Attach an emulator if we can */
	.child_post_bind = dm_scan_fdt_dev,
	.per_child_platdata_auto_alloc_size =
			sizeof(struct pci_child_platdata),
};
